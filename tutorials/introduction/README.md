# Getting started with C

TODO: Specify what versions should be used, try the guide with fresh installs.

### Steps
1. Install Cygwin
2. Install Visual Studio Code
3. Setup Make
4. Setup Visual Studio Code to build
5. Setup Visual Studio Code to debug

### Install Cygwin
You can install Cygwin [here](https://cygwin.com/install.html). Be sure to include `gcc`, `gdb`, and `make` at the `Select Package` step. Start the terminal and verify that everything was installed. You can use the commands

* `gcc --version` 
* `gdb --version` 
* `make --version`


Cygwin install             |  Select packages
:-------------------------:|:-------------------------:
![Verify cygwin](images/verify_cygwin_install.png) |  ![Verify cygwin](images/select_packages_smalll.png)


You also need to add `cygwin\bin` (the full path, in my case `C:\cygwin64\bin`) to your Windows `PATH` variable. [This](https://www.java.com/sv/download/help/path.xml) guide explains how you could do it. 

### Install Visual Studio code
You can install Visual Studio code [here](https://code.visualstudio.com/). After Visual Studio code is installed you can restart your Cygwin terminal and then use the command `code . ` to open VS Code in your working directory. Install the C/C++ extension (`@id:ms-vscode.cpptools`).

![Verify cygwin](images/vscode_c_extension.png)




### Setup Make 
Create a new file via Visual Studio Code called `hello_world.c`. Print `hello world`, or something similar we could use to verify that everything works.

```C
//hello_world.c
#include <stdio.h>

int main() {
    printf("Hello world!");
}

```

The next step is to create a `Makefile` (make sure that the file is called `Makefile`). Create a `target` called `hello_world` with the file `hello_world.c` as a prerequisite. Then we just use the command `gcc -o hello_world.exe hello_world.c` to compile our program. Use [this link](https://makefiletutorial.com/) get more information about `make` and its syntax. The make file should look something like this:

```Make
helloworld: hello_world.c
    gcc -o hello_world.exe hello_world.c 
```

Make sure you have an M logo to the left of the Makefile name. This means Visual Studio Code understands that this is a makefile and will use tabs correctly, else you could have a problem with make complaining that spaces were used instead of tabs.

Verify that it works by writing `make` in the folder you created the `Makefile`.

![Verify make](images/verify_make.png)

### Setup Visual Studio Code to build
Sure, we could use the make command every time we wanna build. But we could also use our editor to do it for us as well. In Visual Studio, go to `Terminal` -> `Configure Default Task Build` -> `Create tasks.json file from template` -> `Others`.

 A `tasks.json` file should be generated. Now we will just specify the task to use `make`.


```JSON
//tasks.json
 {
    "version": "2.0.0",
    "tasks": [
        {
            "type": "shell",
            "label": "Make",
            "command": "make",
            "args": [],
            "options": {},
            "problemMatcher": [],
            "group": {
                "kind": "build",
                "isDefault": true
            }
        },
    ]
}
```

 Verify that it works with the command `ctrl + shift + b`.

 ![Verify make](images/verify_build.png)

### Setup Visual Studio Code to debug
In Visual Studio Code to to `Run` -> `Add configuration` -> `C/C++(GDB/LLDB)` -> `Default Configuration`. This will generate a `launch.json` file. Make sure to update `miDebugerPath` to _*YOUR*_ path and set `stopAtEntry` to `true`.

```JSON
//launch.json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "(gdb) Build and debug active file",
            "type": "cppdbg",
            "request": "launch",
            "program": "${fileDirname}\\${fileBasenameNoExtension}.exe",
            "args": [],
            "stopAtEntry": true,
            "cwd": "${workspaceFolder}",
            "environment": [],
            "externalConsole": false,
            "MIMode": "gdb",
            "miDebuggerPath": "C:\\cygwin64\\bin\\gdb.exe",
            "setupCommands": [
                {
                    "description": "Enable pretty-printing for gdb",
                    "text": "-enable-pretty-printing",
                    "ignoreFailures": true
                }
            ],
            "preLaunchTask": "C/C++: gcc.exe build active file"
        }
    ]
}
```

Verify that debug works by using the command `CTRL + F5` and switch to the debugger view in the left menu. I added a variable just to make sure that my Locals worked as well.

 ![Verify make](images/verify_debug.png)

